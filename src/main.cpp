#include <iostream>
#include "my_hash.h"
#include <string.h>
int main(int argc, char** argv)
{
    std::string help = "This is a file hashing program.\n"
    "Type \"filename --crc\"to get CRC32 hash of your file.\n"
    "Type \"filename --sum\" to get the sum of 32bit blocks of data.";
    if(argc != 3)
        std::cout << help;
    else if(!strcmp(argv[2],"--sum"))
        std::cout << sum_hash(argv[1]) << std::endl;
    else if(!strcmp(argv[2],"--crc"))
        std::cout << crc_hash(argv[1]) << std::endl;
    else std::cout << help;
    return 0;
}
