#ifndef MY_HASH_H
#define MY_HASH_H
#include <fstream>
#include <string>


uint32_t sum_hash(std::string s);
uint32_t crc_hash(std::string s);

#endif // MY_HASH_H
