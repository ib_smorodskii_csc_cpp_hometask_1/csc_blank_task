#include "my_hash.h"

uint32_t sum_hash(std::string s)
{
    std::ifstream f(s);
    if(!f.is_open())
    {
		throw "Error: file is not opened";
    }
    int i = 0;
    int j = 3;
    char c;
    while(!f.eof())
    {
        f>>c;
        i+=((int)c)<<j*8;
        j = (j+3)%4;
    }
    return i;
}


uint32_t crc_hash(std::string s)
{
    std::ifstream f(s);
    if(!f.is_open())
    {
		throw "Error: file is not opened";
    }
    char c;
    uint32_t crc_table[256];
    uint32_t crc;
    for (int i = 0; i < 256; i++)
    {
        crc = i;
        for (int j = 0; j < 8; j++)
        {
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;
        }
        crc_table[i] = crc;
    }
    crc = 0xFFFFFFFFUL;
    while (!f.eof())
    {
        f>>c;
        crc = crc_table[(crc ^ c) & 0xFF] ^ (crc >> 8);
    }
    return crc ^ 0xFFFFFFFFUL;
}
